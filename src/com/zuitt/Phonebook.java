package com.zuitt;

import java.util.ArrayList;
import java.util.List;

public class Phonebook{

    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook(){}


    public ArrayList<Contact> getContacts(){
        return contacts;
    }

    public void setContacts(Contact newContact){
        contacts.add(newContact);
    }
}
