package com.zuitt;

import java.util.ArrayList;

public class Contact{

    private String name;
    private ArrayList <String> numbers = new ArrayList<>();
    private ArrayList <String> address = new ArrayList<>();

    public Contact(){}

    public Contact(String name, ArrayList<String> numbers, ArrayList<String> address){
        this.name = name;
        this.numbers = numbers;
        this.address = address;
    }

    public String getName(){
        return name;
    }
    public ArrayList<String> getAddress(){
        return address;
    }
    public ArrayList<String> getNumber(){
        return numbers;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAddress(String newAddress){
        address.add(newAddress);
    }

    public void setNumbers(String newNumber){
        numbers.add(newNumber);
    }

}
